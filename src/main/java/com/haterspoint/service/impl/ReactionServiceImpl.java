package com.haterspoint.service.impl;

import com.haterspoint.dto.ReactionDTO;
import com.haterspoint.entity.Brand;
import com.haterspoint.entity.Reaction;
import com.haterspoint.entity.UserEntity;
import com.haterspoint.mapper.ReactionMapper;
import com.haterspoint.repository.BrandRepository;
import com.haterspoint.repository.ReactionRepository;
import com.haterspoint.repository.UserRepository;
import com.haterspoint.service.ReactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Slf4j
@Service
public class ReactionServiceImpl implements ReactionService {

    @Autowired
    ReactionRepository reactionRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BrandRepository brandRepository;

    @Override
    public Integer saveReaction(ReactionDTO reactionDTO)
    {
        try
        {
            int userId = reactionDTO.getUserId();
            int brandId = reactionDTO.getProductId();
            UserEntity userEntity = userRepository.findById(userId);
            Brand brand = brandRepository.findById(brandId);

            //to check if the user has already reacted for a product
            int rows = reactionRepository.findByBrandIdAndUserId(brandId,userId).size();

            if(userEntity != null && brand!=null && rows == 0)
            {
                Reaction newReaction = ReactionMapper.convertReactionDTOtoReaction(reactionDTO,userEntity,brand);
                Reaction isReactionSaved = reactionRepository.save(newReaction);
                if(!ObjectUtils.isEmpty(isReactionSaved))
                    return 200;
            }
            else if(rows > 0)
                //report the user about their initial reaction- to be implemented in front end
                return 201;
            return 0;
        }
        catch (Exception ex)
        {
            log.info("Failed at ReactionServiceImpl: "+ ex.getMessage());
            return 0;
        }


    }
}
